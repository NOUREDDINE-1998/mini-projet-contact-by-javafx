package com.example.contactslist;

import DataModel.Contact;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.security.PublicKey;

public class ContactDialog {
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameFiled;
    @FXML
    private TextField phoneNumberField;
    @FXML
    private TextField notesField;

    public Contact getContact(){
        String firstName=firstNameField.getText();
        String lastName=lastNameFiled.getText();
        String phoneNumber=phoneNumberField.getText();
        String notes=notesField.getText();
        Contact newContact= new Contact(firstName,lastName,phoneNumber,notes);
        return newContact;
    }

    public void editContact(Contact item){
        firstNameField.setText(item.getFirstName());
        lastNameFiled.setText(item.getLastName());
        phoneNumberField.setText(item.getPhoneNumber());
        notesField.setText(item.getNotes());

    }
public void updateContact(Contact contact){
        contact.setFirstName(firstNameField.getText());
        contact.setLastName(lastNameFiled.getText());
        contact.setPhoneNumber(phoneNumberField.getText());
        contact.setNotes(notesField.getText());
}

}
