package com.example.contactslist;

import DataModel.Contact;
import DataModel.ContactData;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.util.Optional;

public class Controller {
@FXML
   private TableView<Contact> tableViewContact;

@FXML
private BorderPane mainPane;
private ContactData data;

public void initialize(){
   data= new ContactData();
   tableViewContact.setItems(data.getContacts());

}

public void showAddContactDialog(){

   Dialog<ButtonType> dialog= new Dialog<>();
   dialog.initOwner(mainPane.getScene().getWindow());
   dialog.setTitle("Add New Contact");
   FXMLLoader fxmlLoader= new FXMLLoader();
   fxmlLoader.setLocation(getClass().getResource("contactDialog.fxml"));

   try{
      dialog.getDialogPane().setContent(fxmlLoader.load());
   }catch (Exception e){
      System.out.println("couldn't load the dialog");
      e.printStackTrace();
      return;

   }
   dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
   dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

   Optional<ButtonType> result= dialog.showAndWait();
   if(result.isPresent()&& result.get()==ButtonType.OK){
     ContactDialog contactController= fxmlLoader.getController();
      Contact item= contactController.getContact();
      data.addContact(item);


   }
}

public void showEditContactDialog(){
   Dialog<ButtonType> dialog= new Dialog<>();
   dialog.initOwner(mainPane.getScene().getWindow());
   dialog.setTitle("Edit Contact");
   FXMLLoader fxmlLoader= new FXMLLoader();
   fxmlLoader.setLocation(getClass().getResource("contactDialog.fxml"));

   try{
      dialog.getDialogPane().setContent(fxmlLoader.load());
   }catch (Exception e){
      System.out.println("couldn't load the dialog");
      e.printStackTrace();
      return;

   }
   Contact editContact= tableViewContact.getSelectionModel().getSelectedItem();

   ContactDialog contactController= fxmlLoader.getController();
   contactController.editContact(editContact);

   dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
   dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

   Optional<ButtonType> result= dialog.showAndWait();
   if(result.isPresent()&& result.get()==ButtonType.OK){
       contactController.updateContact(editContact);

   }
}

public void deleteContact(){
   data.deleteContact(tableViewContact.getSelectionModel().getSelectedItem());
}

}