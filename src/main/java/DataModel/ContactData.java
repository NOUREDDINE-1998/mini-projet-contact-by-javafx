package DataModel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ContactData {
    private  static final String CONTACTS_FILE="contacts.xml";

    private static final String CONTACT="contact";
    private static final String FIRST_NAME="first_name";
    private static final String LAST_NAME="last_name";
    private static final String PHONE_NUMBER="phone_number";
    private static final String NOTES="notes";

    private ObservableList<Contact> contacts;

    public  ContactData(){
        contacts= FXCollections.observableArrayList();
    }

    public ObservableList<Contact> getContacts(){
        contacts.add(new Contact("a","a","a","a"));
        contacts.add(new Contact("b","b","b","b"));
        contacts.add(new Contact("c","c","c","c"));
        contacts.add(new Contact("d","d","d","d"));
        return contacts;
    }

    public void addContact(Contact item){
        contacts.add(item);
    }

    public void deleteContact(Contact item){
        contacts.remove(item);
    }


}
