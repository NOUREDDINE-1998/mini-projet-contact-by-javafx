module com.example.contactslist {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.contactslist to javafx.fxml;
    opens DataModel to javafx.fxml;

    exports com.example.contactslist;
    exports DataModel;

}